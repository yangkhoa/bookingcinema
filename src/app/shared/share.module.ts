import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [CommonModule, SweetAlert2Module, FormsModule],
  exports: [SweetAlert2Module, FormsModule]
})
export class ShareModule {}

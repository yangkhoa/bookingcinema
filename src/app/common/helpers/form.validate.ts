import { AbstractControl, FormControl, FormGroup, FormArray, ValidatorFn, ValidationErrors } from '@angular/forms';

export function ValidateField(field: string, submitted: boolean, form: any) {
  if (!form.get(field).valid && (form.get(field).touched || form.get(field).dirty || submitted)) {
    return true;
  } else {
    return false;
  }
}
export function isFieldValid(field: string, submitted: boolean, form: any) {
  return !form.get(field).valid && (form.get(field).touched || form.get(field).dirty || submitted);
}

export function ValidateFieldNoTouched(field: string, submitted: boolean, form: any) {
  if (!form.get(field).valid && submitted === true) {
    return true;
  } else {
    return false;
  }
}

export function isFieldValidNoTouched(field: string, submitted: boolean, form: any) {
  return !form.get(field).valid && submitted === true;
}

export function validateEmptyField(c: FormControl) {
  return c.value && !c.value.trim()
    ? {
        required: {
          valid: false
        }
      }
    : null;
}

export function passwordConfirmingValidator(c: FormGroup): ValidationErrors | null {
  if (c.get('newPassword').value !== c.get('confirmNewPassword').value) {
    return { passwordNotMatch: true };
  } else {
    return null;
  }
}

export function numberRangeValueValidator(min?: number, max?: number): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let invalid = true;
    if (min != null) {
      invalid = +control.value < min && control.value !== null;
    }
    if (max && !invalid) {
      invalid = +control.value > max && control.value !== null;
    }
    return invalid ? { rangeValueRequired: { value: control.value } } : null;
  };
}

export function spacesValidator(): ValidatorFn {
  return (control: AbstractControl): { [key: string]: any } | null => {
    let invalid = false;
    if (control.value === null || control.value.trim() === '') {
      invalid = true;
    }
    return invalid ? { spacesRequired: { value: control.value } } : null;
  };
}

import { ValidatorFn, AbstractControl } from '@angular/forms';

export function emailFormatValidator(): ValidatorFn {
  return function validate(control: AbstractControl) {
    // tslint:disable-next-line: max-line-length
    const regex = new RegExp(
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    if (!regex.test(control.value)) {
      return {
        email: true
      };
    }

    return null;
  };
}

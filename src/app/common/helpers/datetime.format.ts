import { Injectable } from '@angular/core';

@Injectable()
export class DateTimeHelper {
  constructor() {}

  convertDateToUTC(value: number) {
    const date = new Date();
    return new Date(
      Date.UTC(
        date.getFullYear(),
        date.getMonth(),
        date.getDate() + value,
        date.getHours(),
        date.getMinutes(),
        date.getSeconds()
      )
    );
  }

  UTCToLocal(date: Date) {
    return new Date(
      Date.UTC(
        date.getFullYear(),
        date.getMonth(),
        date.getDate(),
        date.getHours(),
        date.getMinutes(),
        date.getSeconds()
      )
    );
  }

  localToUTC(date: Date) {
    const localDate = new Date(date);
    return new Date(
      localDate.getUTCFullYear(),
      localDate.getUTCMonth(),
      localDate.getUTCDate(),
      localDate.getUTCHours(),
      localDate.getUTCMinutes(),
      localDate.getUTCSeconds()
    );
  }
}

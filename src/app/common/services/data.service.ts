import { Injectable, Output, EventEmitter } from '@angular/core';
import { Observable, throwError, BehaviorSubject } from 'rxjs';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { tap, catchError } from 'rxjs/operators';
import { config } from '../config/web.config';
import { Taikhoan } from '../models/taiKhoanVM';
import { TypeNotify } from '../models/type.arlet';
import { ShareService } from './share.service';

let urlAPI = '';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  public currentUserSubject: BehaviorSubject<Taikhoan>;
  public currentUser: Observable<Taikhoan>;

  @Output() datGhe = new EventEmitter();

  constructor(private http: HttpClient) {
    urlAPI = config.domain;
  }

  handleErr(err) {
    switch (err.status) {
      case 500:
        ShareService.showNotification('Thông báo', err.error, TypeNotify.danger);
        break;
      default:
        ShareService.showNotification('Thông báo', err.error, TypeNotify.danger);
        break;
    }
    return throwError(err);
  }

  getAPI(uri: string): Observable<any> {
    return this.http.get(urlAPI + uri).pipe(
      tap(() => {
        // Thành công xử lí loading
      }),
      catchError(err => {
        return this.handleErr(err);
      })
    );
  }

  getAPITest(uri: string): Observable<any> {
    return this.http.get(uri).pipe(
      tap(() => {
        // Thành công xử lí loading
      }),
      catchError(err => {
        console.log(err);

        return this.handleErr(err);
      })
    );
  }

  postAPI(uri: string, data?: object): Observable<any> {
    return this.http
      .post(urlAPI + uri, data, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json-path+json'
        }),
        responseType: 'text'
      })
      .pipe(
        tap(() => {}),
        catchError(err => {
          return this.handleErr(err);
        })
      );
  }

  putAPI(uri: string, data?: object): Observable<any> {
    return this.http
      .put(urlAPI + uri, data, {
        headers: new HttpHeaders({
          'Content-Type': 'application/json-path+json'
        }),
        responseType: 'text'
      })
      .pipe(
        tap(() => {}),
        catchError(err => {
          return this.handleErr(err);
        })
      );
  }

  deleteAPI(uri: string): Observable<any> {
    return this.http.delete(urlAPI + uri).pipe(
      tap(() => {}),
      catchError(err => {
        return this.handleErr(err);
      })
    );
  }
}

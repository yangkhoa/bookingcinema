import { Injectable } from '@angular/core';
import { Observable, throwError, BehaviorSubject } from 'rxjs';
import { Taikhoan } from '../models/taiKhoanVM';
import { Router } from '@angular/router';
import { TypeNotify } from '../models/type.arlet';
import Swal from 'sweetalert2';
import { config } from '../config/web.config';
declare var $: any;
@Injectable({
  providedIn: 'root'
})
export class ShareService {
  public currentUserSubject: BehaviorSubject<Taikhoan>;
  public currentUser: Observable<Taikhoan>;

  constructor(private router: Router) {
    this.currentUserSubject = new BehaviorSubject<Taikhoan>(JSON.parse(localStorage.getItem(config.userLogin)));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public getCurrentUser(): Taikhoan {
    return this.currentUserSubject.value;
  }

  public isAuthenticated(): boolean {
    if (this.currentUserSubject.value != null) {
      return true;
    }
    return false;
  }

  public logOut(): void {
    this.currentUserSubject.next(null);

    localStorage.removeItem(config.userLogin);

    ShareService.showNotification('Thông báo', 'Bạn đã đăng xuất thành công', TypeNotify.info);
  }

  public static showNotification(title, message, type) {
    $.notify(
      {
        icon: 'notifications',
        message: message,
        title: title
      },
      {
        type: type,
        timer: 1500,
        placement: {
          from: 'top',
          align: 'right'
        },
        template:
          '<div data-notify="container" class="col-xl-4 col-lg-4 col-11 col-sm-4 col-md-4 alert alert-{0} alert-with-icon" role="alert">' +
          '<button mat-button  type="button" aria-hidden="true" class="close mat-button" data-notify="dismiss">  <i class="material-icons">close</i></button>' +
          '<i class="material-icons" data-notify="icon">notifications</i> ' +
          '<span data-notify="title">{1}</span> ' +
          '<span data-notify="message">{2}</span>' +
          '<div class="progress" data-notify="progressbar">' +
          '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
          '</div>' +
          '<a href="{3}" target="{4}" data-notify="url"></a>' +
          '</div>'
      }
    );
  }

  public static showMessage(_title: string = 'Thông báo', _text: string, _type: any) {
    return Swal.fire({
      title: _title,
      text: _text,
      type: _type,
      toast: true,
      position: 'top-right',
      showConfirmButton: false,
      timer: 3000
    });
  }

  public static showMessageConfirm(
    _callback,
    _object: any = {},
    _title: string = 'Thông báo',
    _text: string = '',
    _type: any = 'error',
    _confirmButtonText: string = 'Xác nhận',
    _showCancel = true
  ) {
    return Swal.fire({
      title: _title,
      text: _text,
      type: _type,
      showCancelButton: _showCancel,
      confirmButtonColor: 'green',
      cancelButtonColor: '#eee',
      confirmButtonText: _confirmButtonText
    }).then(result => {
      if (result.value) {
        _callback(_object);
      }
    });
  }
}

export class ThongTinDatVe {
  maLichChieu: number;
  danhSachVe: DanhSachVe[];
  taiKhoanNguoiDung: string;
  email: string;
  soDT: string;

  constructor(maLichChieu: number, taiKhoan: string, email: string, soDT: string) {
    this.taiKhoanNguoiDung = taiKhoan;
    this.maLichChieu = maLichChieu;
    this.email = email;
    this.soDT = soDT;
  }
}

class DanhSachVe {
  maGhe: number;
  giaVe: number;
  loaiGhe: string;
  constructor(maGhe: number, giaVe: number, loaiGhe: string) {
    this.maGhe = maGhe;
    this.giaVe = giaVe;
    this.loaiGhe = loaiGhe;
  }
}

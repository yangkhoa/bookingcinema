export class Taikhoan {
  taiKhoan: string;
  matKhau: string;
  email: string;
  soDt: string;
  hoTen: string;
  tokenKey: string;
  maLoaiNguoiDung: string;
  daXoa: number;
  confirmPassword: string;
}

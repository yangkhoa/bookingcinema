export class Phim {
  heThongRapChieu: heThongRapChieu[];
  biDanh: string;
  danhGia: number;
  hinhAnh: string;
  maPhim: number;
  moTa: string;
  ngayKhoiChieu: string;
  date: Date;
  tenPhim: string;
  trailer: string;
  maLoaiPhim: string;
  constructor() {
    this.date = new Date(this.ngayKhoiChieu);
  }
}

class heThongRapChieu {
  cumRapChieu: cumRapChieu[];
  maHeThongRap: string;
  tenHeThongRap: string;
}

class cumRapChieu {
  lichChieuPhim: lichChieuPhim[];
  maCumRap: string;
  tenCumRap: string;
}

class lichChieuPhim {
  maLichChieu: string;
  maRap: string;
  tenRap: string;
  ngayChieuGioChieu: string;
  giaVe: number;
  thoiLuong: number;
}

export class Rap {
  maRap: number;
  tenRap: string;
  maCumRap: string;
  tenCumRap: string;
  maTrangThaiRap: string;
  maHeThongRap: string;
  tenHeThongRap: string;
}

export enum TypeSweetAlert {
  success = 'success',
  error = 'error',
  warning = 'warning',
  info = 'info',
  question = 'question'
}

export enum TypeNotify {
  success = 'success',
  danger = 'danger',
  warning = 'warning',
  info = 'info'
}

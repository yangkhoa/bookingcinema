import { Injectable } from '@angular/core';
import {
  CanActivate,
  CanActivateChild,
  CanLoad,
  Route,
  UrlSegment,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from '@angular/router';
import { Observable } from 'rxjs';
import { Taikhoan } from '../models/taiKhoanVM';
import { config } from '../config/web.config';
import { ShareService } from '../services/share.service';

@Injectable({
  providedIn: 'root'
})
export class IsLoginGuard implements CanActivate, CanActivateChild, CanLoad {
  user: Taikhoan;

  constructor(private router: Router, private shareServices: ShareService) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean | UrlTree | Observable<boolean | UrlTree> | Promise<boolean | UrlTree> {
    if (this.shareServices.isAuthenticated()) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
  }
  canLoad(route: Route, segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    return true;
  }
}

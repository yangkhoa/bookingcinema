import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/data.service';

@Component({
  selector: 'app-slide-page',
  templateUrl: './slide-page.component.html',
  styleUrls: ['./slide-page.component.scss']
})
export class SlidePageComponent implements OnInit {
  subHinhBanner: Subscription;
  danhSachHinhBanner: any[];
  constructor(private dataService: DataService) {}

  getListHinhBanner() {
    const uri = 'QuanLyPhim/LayDanhSachHinhBanner';
    this.subHinhBanner = this.dataService.getAPI(uri).subscribe((data: any[]) => {
      this.danhSachHinhBanner = data;
    });
  }

  ngOnDestroy(): void {
    this.subHinhBanner.unsubscribe();
  }

  ngOnInit() {
    this.getListHinhBanner();
  }
}

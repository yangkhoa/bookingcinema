import { Component, OnInit, SimpleChanges } from '@angular/core';
import { Subscription } from 'rxjs';
import { HeThongRapVM } from 'src/app/common/models/heThongRapVM';
import { DataService } from 'src/app/common/services/data.service';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-lich-chieu',
  templateUrl: './lich-chieu.component.html',
  styleUrls: ['./lich-chieu.component.scss']
})
export class LichChieuComponent implements OnInit {
  subHeThongRap: Subscription;
  subLichChieuHTR: Subscription;
  heThongRap: HeThongRapVM[];
  heThongRapActive = 'CGV';
  lichChieuTheoHeThongRap: any;

  constructor(private dataService: DataService, private isLoadingService: IsLoadingService) {}

  ngOnInit() {
    this.getListHeThongRap();
    this.getListCumRap(this.heThongRapActive);
  }
  ngOnDestroy() {
    this.subHeThongRap.unsubscribe();
    this.subLichChieuHTR.unsubscribe();
  }

  changeHTR(maHTR) {
    this.heThongRapActive = maHTR;
    this.getListCumRap(this.heThongRapActive);
  }

  getListHeThongRap() {
    const uri = 'QuanLyRap/LayThongTinHeThongRap';
    this.subHeThongRap = this.isLoadingService.add(
      this.dataService.getAPI(uri).subscribe((data: HeThongRapVM[]) => {
        this.heThongRap = data;
      })
    );
  }

  getListCumRap(maHTR) {
    const uri = `QuanLyRap/LayThongTinLichChieuHeThongRap?maHeThongRap=${maHTR}`;
    this.subLichChieuHTR = this.isLoadingService.add(
      this.dataService.getAPI(uri).subscribe((data: any) => {
        this.lichChieuTheoHeThongRap = data;
      })
    );
  }
}

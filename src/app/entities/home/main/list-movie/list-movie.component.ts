import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/data.service';
import { Phim } from 'src/app/common/models/PhimVM';
import { IsLoadingService } from '@service-work/is-loading';
@Component({
  selector: 'app-list-movie',
  templateUrl: './list-movie.component.html',
  styleUrls: ['./list-movie.component.scss']
})
export class ListMovieComponent implements OnInit {
  subDanhSachPhim: Subscription;
  subDanhSachPhimSapChieu: Subscription;
  danhSachPhim: Phim[];
  danhSachPhimSapChieu: Phim[];
  constructor(private dataService: DataService, private isLoadingService: IsLoadingService) {}

  getListMovie() {
    const uri = 'QuanLyPhim/LayDanhSachPhim';
    this.subDanhSachPhim = this.isLoadingService.add(
      this.dataService.getAPI(uri).subscribe((data: Phim[]) => {
        this.danhSachPhim = data;
      })
    );
  }

  getListMovieSapChieu() {
    const uri = 'QuanLyPhim/LayDanhSachPhimSapChieu';
    this.subDanhSachPhimSapChieu = this.isLoadingService.add(
      this.dataService.getAPI(uri).subscribe((data: Phim[]) => {
        this.danhSachPhimSapChieu = data;
      })
    );
  }

  viewPhimComming(){
    this.getListMovieSapChieu();
  }

  ngOnInit() {
    this.getListMovie();
  }

  ngOnDestroy() {
    this.subDanhSachPhim.unsubscribe();
    if (this.subDanhSachPhimSapChieu!=null){
      this.subDanhSachPhimSapChieu.unsubscribe();
    }
   
  }
}

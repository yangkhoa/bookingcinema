import { Component, OnInit, Input } from '@angular/core';
import { Phim } from 'src/app/common/models/PhimVM';
import { Router } from '@angular/router';

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.scss']
})
export class MovieComponent implements OnInit {
  @Input() phim: Phim;

  currentDate: number = Date.now();

  constructor(private router: Router) {}

  isComming(ngayKhoiChieu, currentDate) {
    let ngayKhoiChieuN = new Date(ngayKhoiChieu).getTime();

    return ngayKhoiChieuN < currentDate;
  }

  goToDetail(routing) {
    this.router.navigate([routing]);
  }

  array(n: number): any[] {
    return Array(Math.floor(n));
  }

  ngOnInit() {}
}

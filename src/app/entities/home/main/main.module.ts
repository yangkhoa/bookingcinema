import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingModule } from './main-routing.module';
import { MaterialModule } from 'src/app/shared/material/material.module';

//**COMPONENT */
import { MainComponent } from './main.component';
import { SlidePageComponent } from './slide-page/slide-page.component';
import { ShorcutBookingComponent } from './shorcut-booking/shorcut-booking.component';
import { ListMovieComponent } from './list-movie/list-movie.component';
import { MovieComponent } from './list-movie/movie/movie.component';
import { LichChieuComponent } from './lich-chieu/lich-chieu.component';

@NgModule({
  declarations: [MainComponent, SlidePageComponent, ShorcutBookingComponent, ListMovieComponent, MovieComponent, LichChieuComponent],
  imports: [CommonModule, MainRoutingModule, MaterialModule]
})
export class MainModule {}

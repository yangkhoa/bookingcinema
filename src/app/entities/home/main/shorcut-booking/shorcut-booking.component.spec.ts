import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShorcutBookingComponent } from './shorcut-booking.component';

describe('ShorcutBookingComponent', () => {
  let component: ShorcutBookingComponent;
  let fixture: ComponentFixture<ShorcutBookingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShorcutBookingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShorcutBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { IsLoginGuard } from 'src/app/common/guards/is-login.guard';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: '',
        loadChildren: './main/main.module#MainModule'
      },
      {
        path: 'chi-tiet-phim/:param',
        loadChildren: './movie-detail/movie-detail.module#MovieDetailModule'
      },
      {
        path: 'chi-tiet-dat-ve/:maLichChieu',
        loadChildren: './booking-detail/booking-detail.module#BookingDetailModule',
        canActivate: [IsLoginGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule {}

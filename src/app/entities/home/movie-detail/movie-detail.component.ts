import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/common/services/data.service';
import { Subscription } from 'rxjs';
import { Phim } from 'src/app/common/models/PhimVM';
import { IsLoadingService } from '@service-work/is-loading';

@Component({
  selector: 'app-movie-detail',
  templateUrl: './movie-detail.component.html',
  styleUrls: ['./movie-detail.component.scss']
})
export class MovieDetailComponent implements OnInit {
  public maPhim: number = 0;
  public param: string;
  public phim: Phim;
  constructor(
    private activatedRoute: ActivatedRoute,
    private dataService: DataService,
    private isLoadingService: IsLoadingService
  ) {}

  subPrams: Subscription;
  subChiTietPhim: Subscription;
  ngOnDestroy() {
    this.subPrams.unsubscribe();
    this.subChiTietPhim.unsubscribe();
  }

  ngOnInit() {
    this.subPrams = this.activatedRoute.params.subscribe(prams => {
      this.param = prams.param;

      this.maPhim = this.getMaPhim(this.param);
      if (this.maPhim === 0) {
        alert('Đã có lỗi xảy ra');
      }
    });

    this.layThongTinChiTietPhim(this.maPhim);
  }

  getMaPhim(param) {
    if (param === '' || param == null) {
      return 0;
    }
    return param.split('-').pop();
  }

  layThongTinChiTietPhim(maPhim) {
    const uriLayThongTinChiTiet = `QuanLyRap/LayThongTinLichChieuPhim?MaPhim=${maPhim}`;

    this.subChiTietPhim = this.isLoadingService.add(
      this.dataService.getAPI(uriLayThongTinChiTiet).subscribe((data: Phim) => {
        this.phim = data;
      })
    );
  }

  array(n: number): any[] {
    return Array(Math.floor(n));
  }
}

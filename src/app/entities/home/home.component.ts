import { Component, OnInit, HostListener } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public isMobile: boolean;
  public innerWidth: number;

  constructor() {}

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.innerWidth = window.innerWidth;
  }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth > 991) {
      this.isMobile = false;
    } else {
      this.isMobile = true;
    }
  }

  ngDoCheck() {
    if (this.innerWidth != window.innerWidth) {
      this.innerWidth = window.innerWidth;
    }
    if (this.innerWidth > 991) {
      this.isMobile = false;
    } else {
      this.isMobile = true;
    }
  }
}

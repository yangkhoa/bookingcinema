import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { HomeRoutingModule } from './home-routing.module';
import { HeaderComponent } from 'src/app/entities/home/components/header/header.component';
import { FooterComponent } from 'src/app/entities/home/components/footer/footer.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { SidebarComponent } from 'src/app/entities/home/components/sidebar/sidebar.component';

@NgModule({
  declarations: [HomeComponent, HeaderComponent, FooterComponent, SidebarComponent],
  imports: [CommonModule, HomeRoutingModule, MaterialModule]
})
export class HomeModule {}

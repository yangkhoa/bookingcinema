import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  constructor() {}

  ngOnInit() {}

  isChiTietDatVe() {
    if (location.pathname.indexOf('chi-tiet-dat-ve') > 0) {
      return false;
    }
    return true;
  }
}

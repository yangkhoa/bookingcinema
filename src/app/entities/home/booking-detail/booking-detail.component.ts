import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/common/services/data.service';
import { Subscription } from 'rxjs';
import { ShareService } from 'src/app/common/services/share.service';
import { TypeNotify } from 'src/app/common/models/type.arlet';
import { config } from 'src/app/common/config/web.config';
import { ThongTinDatVe } from 'src/app/common/models/thongTinDatVeVM';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IsLoadingService } from '@service-work/is-loading';
import { MatStepper } from '@angular/material';
import { LoaiGhe } from 'src/app/common/models/loaiGheVM';
import { Taikhoan } from 'src/app/common/models/taiKhoanVM';

@Component({
  selector: 'app-booking-detail',
  templateUrl: './booking-detail.component.html',
  styleUrls: ['./booking-detail.component.scss']
})
export class BookingDetailComponent implements OnInit, OnDestroy {
  subPram: Subscription;
  subService: Subscription;

  maLichChieu: number;

  @ViewChild('stepper', { static: false }) private myStepper: MatStepper;

  danhSachChonLoaiGhe: any[] = [];
  danhSachLoaiGhe: LoaiGhe[] = [];
  danhSachGheDangDat: any[] = [];
  lichChieu: any = {};

  tongTien = 0;
  tongTienChonVe = 0;
  quantityThuong = 0;
  quantityVip = 0;
  isSuccessBooking = false;
  userLogin: Taikhoan;

  isChooseMethodPay = false;

  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  counter: number = 300;
  interval;
  constructor(
    private router: Router,
    private atvRoute: ActivatedRoute,
    private dataService: DataService,
    private formBuilder: FormBuilder,
    private isLoadingService: IsLoadingService
  ) {}

  startTiming() {
    this.counter = 10;
    var id = (this.interval = setInterval(() => {
      if (this.counter > 0) {
        this.counter--;
      } else {
      }
      if (this.counter == 0) {
        clearInterval(id);
        ShareService.showMessageConfirm(
          () => {
            this.myStepper.previous();
          },
          null,
          'Bạn đã quá thời gian đặt vé cho phép!',
          null,
          'warning',
          'Đặt vé lại',
          false
        );
      }
    }, 1000));
  }

  ngOnInit() {
    this.subPram = this.atvRoute.params.subscribe(param => {
      this.layThongTinLichChieu(param.maLichChieu);
      this.maLichChieu = param.maLichChieu;
    });

    this.dataService.datGhe.subscribe(gheDangDat => {
      if (gheDangDat.dangDat) {
        this.danhSachGheDangDat.push(gheDangDat);
      } else {
        let index = this.danhSachGheDangDat.findIndex(ghe => ghe.maGhe === gheDangDat.maGhe);
        if (index !== -1) {
          this.danhSachGheDangDat.splice(index, 1);
        }
      }
      //console.log('mảng ghế đang đặt', this.danhSachGheDangDat);
      this.tinhTongTien();
    });

    this.userLogin = JSON.parse(localStorage.getItem(config.userLogin));

    /// STEP1: CHỌN LOẠI VÉ
    this.firstFormGroup = this.formBuilder.group({
      firstCtrl: [this.tongTienChonVe > 0]
    });

    /// STEP2: CHỌN GHẾ & THANH TOÁN
    this.secondFormGroup = this.formBuilder.group({
      secondCtrl: [this.isSuccessBooking]
    });
  }

  ngOnDestroy() {
    this.subPram.unsubscribe();
    this.subService.unsubscribe();
  }

  tinhTongTien() {
    let tongTien = 0;
    this.danhSachGheDangDat.forEach((ghe, index) => {
      tongTien += ghe.giaVe;
    });
    this.tongTien = tongTien;
  }

  layThongTinLichChieu(maLichChieu: number) {
    const uriLayDanhSachPhongVe = `QuanLyDatVe/LayDanhSachPhongVe?MaLichChieu=${maLichChieu}`;

    this.subService = this.isLoadingService.add(
      this.dataService.getAPI(uriLayDanhSachPhongVe).subscribe(data => {
        this.lichChieu = data;
      })
    );
  }

  datVe() {
    this.danhSachLoaiGhe = [];

    this.danhSachGheDangDat.map(item => {
      this.danhSachLoaiGhe.push(item.maLoaiGhe);
    });

    if (this.danhSachGheDangDat.length === 0) {
      ShareService.showNotification('Thông báo', 'Bạn chưa chọn ghế !', TypeNotify.danger);
      return;
    }

    if (this.danhSachChonLoaiGhe.length !== this.danhSachGheDangDat.length) {
      ShareService.showNotification('Thông báo', 'Bạn chọn đủ số ghế đã chọn trước đó !', TypeNotify.danger);
      return;
    } else {
      let uniqueDSGDD = this.uniqueArray(this.danhSachLoaiGhe);
      let uniqueDSCG = this.uniqueArray(this.danhSachChonLoaiGhe);

      if (uniqueDSCG.length !== uniqueDSGDD.length) {
        ShareService.showNotification('Thông báo', 'Bạn chưa chọn đúng loại ghế đã chọn trước đó !', TypeNotify.danger);
        return;
      } else {
        let differ = uniqueDSGDD.filter(function(word) {
          return !uniqueDSCG.includes(word);
        });
        if (differ.length !== 0) {
          ShareService.showNotification(
            'Thông báo',
            'Bạn chưa chọn đúng loại ghế đã chọn trước đó !',
            TypeNotify.danger
          );
          return;
        }
      }
    }

    if (!this.isChooseMethodPay) {
      ShareService.showNotification('Thông báo', 'Bạn chưa chọn phương thức thanh toán !', TypeNotify.danger);
      return;
    }

    let ttDatVe = new ThongTinDatVe(
      this.lichChieu.thongTinPhim.maLichChieu,
      this.userLogin.taiKhoan,
      this.userLogin.email,
      this.userLogin.soDt
    );

    ttDatVe.danhSachVe = this.danhSachGheDangDat;

    let msg = ' Bạn đã đặt ghế: ';
    this.danhSachGheDangDat.map(item => {
      msg += item.stt + ',';
    });

    ShareService.showMessageConfirm(
      () => {
        this.bookingPost(ttDatVe).subscribe(
          result => {
            this.isSuccessBooking = true;
            this.myStepper.next();
            this.layThongTinLichChieu(this.lichChieu.thongTinPhim.maLichChieu);
            this.danhSachGheDangDat = [];
            this.tongTien = 0;
          },
          error => {
            console.log(error);
          }
        );
      },
      this.myStepper,
      'Xác nhận đặt vé',
      msg,
      'success'
    );
  }

  bookingPost(ttDatVe: ThongTinDatVe) {
    const uriQuanLyDatVe = `QuanLyDatVe/DatVe`;
    let object: any = this.dataService.postAPI(uriQuanLyDatVe, ttDatVe);
    return object;
  }

  addTicket(maLoaiGhe: string, giaVe: number) {
    this.danhSachChonLoaiGhe.push(maLoaiGhe);
    if (maLoaiGhe === 'Vip') {
      this.quantityVip++;
    } else {
      this.quantityThuong++;
    }
    this.tongTienChonVe += giaVe;
  }

  removeTicket(maLoaiGhe: string, giaVe: number) {
    let index = this.danhSachChonLoaiGhe.findIndex(ml => ml === maLoaiGhe);
    if (index !== -1) {
      this.danhSachChonLoaiGhe.splice(index, 1);
    }
    if (maLoaiGhe === 'Vip' && this.quantityVip > 0) {
      this.quantityVip--;
      this.tongTienChonVe -= giaVe;
    }
    if (maLoaiGhe === 'Thuong' && this.quantityThuong > 0) {
      this.quantityThuong--;
      this.tongTienChonVe -= giaVe;
    }
  }

  uniqueArray(a) {
    function onlyUnique(value, index, self) {
      return self.indexOf(value) === index;
    }
    let unique = a.filter(onlyUnique);
    return unique;
  }

  chooseMethodPay() {
    this.isChooseMethodPay = true;
  }
}

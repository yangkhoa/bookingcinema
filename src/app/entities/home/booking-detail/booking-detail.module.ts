import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookingDetailComponent } from './booking-detail.component';
import { BookingDetailRoutingModule } from './booking-detail-routing.module';
import { BookingChairComponent } from './booking-chair/booking-chair.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { PipeminutePipe } from 'src/app/common/pipe/pipeminute.pipe';

@NgModule({
  declarations: [BookingDetailComponent, BookingChairComponent, PipeminutePipe],
  imports: [CommonModule, BookingDetailRoutingModule, MaterialModule, FormsModule, ReactiveFormsModule]
})
export class BookingDetailModule {}

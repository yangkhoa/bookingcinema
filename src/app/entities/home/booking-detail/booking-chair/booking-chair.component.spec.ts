import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BookingChairComponent } from './booking-chair.component';

describe('BookingChairComponent', () => {
  let component: BookingChairComponent;
  let fixture: ComponentFixture<BookingChairComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BookingChairComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BookingChairComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

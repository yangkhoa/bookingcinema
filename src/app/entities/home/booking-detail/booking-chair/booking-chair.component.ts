import { Component, OnInit, Input } from '@angular/core';
import { DataService } from 'src/app/common/services/data.service';

@Component({
  selector: 'app-booking-chair',
  templateUrl: './booking-chair.component.html',
  styleUrls: ['./booking-chair.component.scss']
})
export class BookingChairComponent implements OnInit {
  @Input() gheInput: any = {};
  dangDat: boolean = false;
  constructor(private dataServices: DataService) {}

  ngOnInit() {}
  datGhe() {
    this.dangDat = !this.dangDat;
    let gheDangDat: any = {
      maGhe: this.gheInput.maGhe,
      stt: this.gheInput.stt,
      giaVe: this.gheInput.giaVe,
      maLoaiGhe: this.gheInput.loaiGhe,
      dangDat: this.dangDat
    };
    this.dataServices.datGhe.emit(gheDangDat);
  }
}

import { Component, OnInit, ViewChild } from '@angular/core';
import { Taikhoan } from 'src/app/common/models/taiKhoanVM';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/data.service';
import { MatTableDataSource, MatPaginator, MatSort } from '@angular/material';
import { ShareService } from 'src/app/common/services/share.service';
import { TypeNotify } from 'src/app/common/models/type.arlet';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {
  subDanhSachNguoiDung: Subscription;
  subLoaiNguoiDung: Subscription;
  subUser: Subscription;

  user: Taikhoan = new Taikhoan();

  isEdit = false;
  isInsert = false;

  title = 'Danh sách người dùng';
  subTitle = 'Hihi';

  danhSachNguoiDung: Taikhoan[] = [];
  displayedColumns: string[] = ['taiKhoan', 'hoTen', 'email', 'soDt', 'maLoaiNguoiDung', 'actions'];
  loaiNguoiDung: any[] = [];

  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private dataService: DataService) {}

  getFullListUser() {
    const uriDanhSachNguoiDung = 'QuanLyNguoiDung/LayDanhSachNguoiDung';
    this.subDanhSachNguoiDung = this.dataService.getAPI(uriDanhSachNguoiDung).subscribe((data: Taikhoan[]) => {
      this.danhSachNguoiDung = data;
      this.dataSource.data = data;
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  getTypeUser() {
    const uriLoaiNguoiDung = 'QuanLyNguoiDung/LayDanhSachLoaiNguoiDung';
    this.subLoaiNguoiDung = this.dataService.getAPI(uriLoaiNguoiDung).subscribe((data: any[]) => {
      this.loaiNguoiDung = data;
    });
  }

  ngOnInit() {
    this.getFullListUser();
    this.getTypeUser();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  ngOnDestroy(): void {
    this.subDanhSachNguoiDung.unsubscribe();
    if (this.subUser != null) {
      this.subUser.unsubscribe();
    }
    if (this.subLoaiNguoiDung != null) {
      this.subLoaiNguoiDung.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  backToFormData() {
    this.isEdit = false;
    this.isInsert = false;
    this.user = new Taikhoan();
  }

  confirmUpdate() {
    if (this.isEdit == true) {
      const uriCapNhatNguoiDung = 'QuanLyNguoiDung/CapNhatThongTinNguoiDung';

      this.dataService.putAPI(uriCapNhatNguoiDung, this.user).subscribe((data: any) => {
        this.getFullListUser();
        ShareService.showNotification('Cập nhật thông tin người dùng thành công', '', TypeNotify.success);
        this.isEdit = false;
      });
    }

    if (this.isInsert == true) {
      const uriThemNguoiDung = 'QuanLyNguoiDung/DangKy';
      this.dataService.postAPI(uriThemNguoiDung, this.user).subscribe((data: any) => {
        this.getFullListUser();
        ShareService.showNotification('Thêm người dùng thành công', '', TypeNotify.success);
        this.isInsert = false;
      });
    }
  }

  insertUser() {
    this.isInsert = true;

    this.user = new Taikhoan();
  }

  editUser(taiKhoan: number) {
    const uriGetUser = `QuanLyNguoiDung/LayDanhSachNguoiDung?taiKhoan=${taiKhoan}`;
    this.subUser = this.dataService.getAPI(uriGetUser).subscribe((data: Taikhoan) => {
      this.user = data;
      this.isEdit = true;
    });
  }

  xoaUser(taiKhoan: number) {
    const uriDeleteUser = `QuanLyNguoiDung/XoaNguoiDung?TaiKhoan=${taiKhoan}`;
    ShareService.showMessageConfirm(
      () => {
        this.dataService.deleteAPI(uriDeleteUser).subscribe(
          result => {
            this.getFullListUser();
          },
          error => {
            console.log(error);
          }
        );
      },
      null,
      `Bạn có chắc chắn muốn xóa tài khoản này không?`
    );
  }
}

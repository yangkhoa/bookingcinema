import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserManagementRoutingModule } from './user-management-routing.module';
import { UserManagementComponent } from './user-management.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ComponentsModule } from '../components/components.module';
import { ShareModule } from 'src/app/shared/share.module';

@NgModule({
  declarations: [UserManagementComponent],
  imports: [CommonModule, UserManagementRoutingModule, MaterialModule, ComponentsModule, ShareModule]
})
export class UserManagementModule {}

import { Component, OnInit } from '@angular/core';
import { ShareService } from 'src/app/common/services/share.service';
import { Router } from '@angular/router';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: './dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  { path: './quan-ly-nguoi-dung', title: 'Quản lý Người dùng', icon: 'face', class: '' },
  { path: './quan-ly-phim', title: 'Quản lý Phim', icon: 'movie', class: '' },
  { path: './quan-ly-rap', title: 'Quản lý Rạp chiếu', icon: 'home', class: '' },
  { path: './quan-ly-cum-rap', title: 'Quản lý Cụm rạp chiếu', icon: 'room', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];

  constructor(private shareService: ShareService, private router: Router) {}

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  }

  logOut() {
    this.shareService.logOut();
    this.router.navigate(['/']);
  }
}

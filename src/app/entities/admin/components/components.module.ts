import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { DataTableComponent } from './data-table/data-table.component';
import { MaterialModule } from 'src/app/shared/material/material.module';

@NgModule({
  imports: [CommonModule, RouterModule, MaterialModule],
  declarations: [FooterComponent, NavbarComponent, SidebarComponent, DataTableComponent],
  exports: [FooterComponent, NavbarComponent, SidebarComponent, DataTableComponent]
})
export class ComponentsModule {}

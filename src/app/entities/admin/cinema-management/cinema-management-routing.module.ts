import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CinemaManagementComponent } from './cinema-management.component';

const routes: Routes = [
  {
    path: '',
    component: CinemaManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CinemaManagementRoutingModule {}

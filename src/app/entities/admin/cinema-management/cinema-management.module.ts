import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../components/components.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ShareModule } from 'src/app/shared/share.module';
import { CinemaManagementComponent } from './cinema-management.component';
import { CinemaManagementRoutingModule } from './cinema-management-routing.module';

@NgModule({
  declarations: [CinemaManagementComponent],
  imports: [CommonModule, ComponentsModule, MaterialModule, CinemaManagementRoutingModule, ShareModule]
})
export class CinemaManagementModule {}

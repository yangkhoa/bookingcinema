import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/data.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { ShareService } from 'src/app/common/services/share.service';
import { TypeNotify } from 'src/app/common/models/type.arlet';
import { IsLoadingService } from '@service-work/is-loading';
import { Rap } from 'src/app/common/models/RapVM';
import { CumRap } from 'src/app/common/models/cumRapVM';

@Component({
  selector: 'app-cinema-management',
  templateUrl: './cinema-management.component.html',
  styleUrls: ['./cinema-management.component.scss']
})
export class CinemaManagementComponent implements OnInit {
  subDanhSachRap: Subscription;
  subTrangThaiRap: Subscription;
  subRap: Subscription;
  subCumRap: Subscription;

  rap: Rap = new Rap();
  isEdit = false;
  isInsert = false;

  title = 'Danh sách rạp';
  subTitle = 'Hihi';

  danhSachRap: Rap[] = [];
  displayedColumns: string[] = ['tenRap', 'maTrangThaiRap', 'maCumRap', 'actions'];
  trangThaiRap: any[] = [];
  cumRapChieu: CumRap[] = [];

  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private dataService: DataService, private isLoadingService: IsLoadingService) {}

  getListCumRap() {
    const uriDanhSachCumRap = 'QuanLyRap/LayThongTinCumRapChieu';
    this.subCumRap = this.isLoadingService.add(
      this.dataService.getAPI(uriDanhSachCumRap).subscribe((data: CumRap[]) => {
        this.cumRapChieu = data;
      })
    );
  }

  getFullListRap() {
    const uriDanhSachRap = 'QuanLyRap/LayThongTinRapChieu';
    this.subDanhSachRap = this.isLoadingService.add(
      this.dataService.getAPI(uriDanhSachRap).subscribe((data: Rap[]) => {
        this.danhSachRap = data;
        this.dataSource.data = data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    );
  }

  getTypeRap() {
    const uriTrangThaiRap = 'QuanLyRap/LayDanhSachTrangThaiRap';
    this.subTrangThaiRap = this.dataService.getAPI(uriTrangThaiRap).subscribe((data: any[]) => {
      this.trangThaiRap = data;
    });
  }

  ngOnDestroy(): void {
    this.subDanhSachRap.unsubscribe();
    if (this.subRap != null) {
      this.subRap.unsubscribe();
    }
    if (this.subTrangThaiRap != null) {
      this.subTrangThaiRap.unsubscribe();
    }
    if (this.subCumRap != null) {
      this.subCumRap.unsubscribe();
    }
  }

  ngOnInit() {
    this.getListCumRap();
    this.getFullListRap();
    this.getTypeRap();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  backToFormData() {
    this.isEdit = false;
    this.isInsert = false;
    this.rap = new Rap();
  }

  confirmUpdate() {
    if (this.isEdit == true) {
      const uriCapNhatNguoiDung = 'QuanLyRap/CapNhatThongTinRapChieu';

      this.isLoadingService.add(
        this.dataService.putAPI(uriCapNhatNguoiDung, this.rap).subscribe((data: any) => {
          this.getFullListRap();
          ShareService.showNotification('Cập nhật thông tin rạp thành công', '', TypeNotify.success);
          this.isEdit = false;
        })
      );
    }

    if (this.isInsert == true) {
      const uriThemNguoiDung = 'QuanLyRap/ThemRapChieu';

      this.isLoadingService.add(
        this.dataService.postAPI(uriThemNguoiDung, this.rap).subscribe((data: any) => {
          this.getFullListRap();
          ShareService.showNotification('Thêm rạp thành công', '', TypeNotify.success);
          this.isInsert = false;
        })
      );
    }
  }

  insertRap() {
    this.isInsert = true;
    this.rap = new Rap();
  }

  editRap(maRap: number) {
    const uriGetRap = `QuanLyRap/LayThongTinRapChieu?maRap=${maRap}`;
    this.subRap = this.dataService.getAPI(uriGetRap).subscribe((data: Rap) => {
      this.rap = data;
      this.isEdit = true;
    });
  }

  xoaRap(maRap: number) {
    const uriDeleteRap = `QuanLyRap/XoaRapChieu?maRap=${maRap}`;
    ShareService.showMessageConfirm(
      () => {
        this.isLoadingService.add(
          this.dataService.deleteAPI(uriDeleteRap).subscribe(
            result => {
              this.getFullListRap();
            },
            error => {
              console.log(error);
            }
          )
        );
      },
      null,
      `Bạn có chắc chắn muốn xóa rạp này không?`
    );
  }
}

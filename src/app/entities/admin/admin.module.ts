import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ComponentsModule } from './components/components.module';
import { AdminComponent } from './admin.component';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ShareModule } from 'src/app/shared/share.module';

@NgModule({
  declarations: [AdminComponent],
  imports: [
    ComponentsModule,
    CommonModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    ShareModule
  ]
})
export class AdminModule {}

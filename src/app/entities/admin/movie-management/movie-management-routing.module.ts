import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MovieComponent } from '../../home/main/list-movie/movie/movie.component';
import { MovieManagementComponent } from './movie-management.component';

const routes: Routes = [
  {
    path: '',
    component: MovieManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MovieManagementRoutingModule {}

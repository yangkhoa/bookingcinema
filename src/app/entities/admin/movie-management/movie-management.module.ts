import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MovieManagementComponent } from './movie-management.component';
import { ComponentsModule } from '../components/components.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { MovieManagementRoutingModule } from './movie-management-routing.module';
import { ShareModule } from 'src/app/shared/share.module';

@NgModule({
  declarations: [MovieManagementComponent],
  imports: [CommonModule, ComponentsModule, MaterialModule, MovieManagementRoutingModule, ShareModule]
})
export class MovieManagementModule {}

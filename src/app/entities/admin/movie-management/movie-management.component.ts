import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { Phim } from 'src/app/common/models/PhimVM';
import { DataService } from 'src/app/common/services/data.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { ShareService } from 'src/app/common/services/share.service';
import { TypeNotify } from 'src/app/common/models/type.arlet';
import { IsLoadingService } from '@service-work/is-loading';
import { HttpRequest, HttpClient, HttpEventType, HttpResponse } from '@angular/common/http';
import { config } from 'src/app/common/config/web.config';

@Component({
  selector: 'app-movie-management',
  templateUrl: './movie-management.component.html',
  styleUrls: ['./movie-management.component.scss']
})
export class MovieManagementComponent implements OnInit {
  subDanhSachPhim: Subscription;
  subPhim: Subscription;
  subLoaiPhim: Subscription;

  phim: Phim = new Phim();

  isEdit = false;
  isInsert = false;

  title = 'Danh sách phim';
  subTitle = 'Hihi';

  public progress: number;
  public message: string;

  danhSachPhim: Phim[] = [];
  displayedColumns: string[] = [
    'tenPhim',
    'hinhAnh',
    'moTa',
    'biDanh',
    'danhGia',
    'maLoaiPhim',
    'trailer',
    'ngayKhoiChieu',
    'actions'
  ];
  loaiPhim: any[] = [];

  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private dataService: DataService, private isLoadingService: IsLoadingService, private http: HttpClient) {}

  getListMovie() {
    const uriDanhSachPhim = 'QuanLyPhim/LayDanhSachPhimDayDu';
    this.subDanhSachPhim = this.isLoadingService.add(
      this.dataService.getAPI(uriDanhSachPhim).subscribe((data: Phim[]) => {
        this.danhSachPhim = data;
        this.dataSource.data = data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    );
  }

  getTypeMovie() {
    const uriLoaiPhim = 'QuanLyPhim/LayDanhSachLoaiPhim';
    this.subLoaiPhim = this.isLoadingService.add(
      this.dataService.getAPI(uriLoaiPhim).subscribe((data: any[]) => {
        this.loaiPhim = data;
      })
    );
  }

  ngOnInit() {
    this.getListMovie();
    this.getTypeMovie();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }
  ngOnDestroy(): void {
    this.subDanhSachPhim.unsubscribe();
    if (this.subPhim != null) {
      this.subPhim.unsubscribe();
    }
    if (this.subLoaiPhim != null) {
      this.subLoaiPhim.unsubscribe();
    }
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  confirmUpdate() {
    if (this.isEdit === true) {
      const uriCapNhatPhim = 'QuanLyPhim/CapNhatPhim';

      this.phim.ngayKhoiChieu = this.phim.date.toLocaleDateString('en-GB', { timeZone: 'UTC', hour12: false });

      this.isLoadingService.add(
        this.dataService.putAPI(uriCapNhatPhim, this.phim).subscribe((data: any) => {
          this.getListMovie();
          ShareService.showNotification('Cập nhật phim thành công', '', TypeNotify.success);
          this.isEdit = false;
        })
      );
    }

    if (this.isInsert === true) {
      const uriThemPhim = 'QuanLyPhim/ThemPhim';
      this.phim.ngayKhoiChieu = this.phim.date.toLocaleDateString('en-GB', { timeZone: 'UTC', hour12: false });

      this.isLoadingService.add(
        this.dataService.postAPI(uriThemPhim, this.phim).subscribe((data: any) => {
          this.getListMovie();
          ShareService.showNotification('Thêm phim thành công', '', TypeNotify.success);
          this.isInsert = false;
        })
      );
    }
  }

  backToFormData() {
    this.isEdit = false;
    this.isInsert = false;
    this.phim = new Phim();
  }

  insertPhim() {
    this.phim = new Phim();
    this.isInsert = true;
    // MỞ FORM INSERT
  }

  editPhim(maPhim: number) {
    const urlGetPhim = `QuanLyPhim/LayDanhSachPhim?maPhim=${maPhim}`;
    this.subPhim = this.isLoadingService.add(
      this.dataService.getAPI(urlGetPhim).subscribe((data: Phim) => {
        this.phim = data;
        this.phim.date = new Date(data.ngayKhoiChieu);
        this.isEdit = true;
      })
    );
  }

  xoaPhim(maPhim: number) {
    const uriDeletePhim = `QuanLyPhim/XoaPhim?maPhim=${maPhim}`;
    ShareService.showMessageConfirm(
      () => {
        this.dataService.deleteAPI(uriDeletePhim).subscribe(
          result => {
            this.getListMovie();
          },
          error => {
            console.log(error);
          }
        );
      },
      null,
      `Bạn có chắc chắn muốn xóa phim này không?`
    );
  }

  upload(files) {
    if (files.length === 0) {
      return;
    }

    const formData = new FormData();

    for (let file of files) {
      formData.append(file.name, file);
    }

    const uploadReq = new HttpRequest('POST', `${config.domain}QuanLyPhim/UploadAnhPhim`, formData, {
      reportProgress: true
    });

    this.http.request(uploadReq).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress = Math.round((100 * event.loaded) / event.total);
      } else if (event.type === HttpEventType.Response) {
        this.message = event.body.toString();
        this.phim.hinhAnh = this.message;
        ShareService.showNotification('Upload ảnh phim thành công', '', TypeNotify.success);
      }
    });
  }
}

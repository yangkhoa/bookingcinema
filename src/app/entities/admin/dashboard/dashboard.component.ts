import { Component, OnInit } from '@angular/core';
import * as Chartist from 'chartist';
import { DataService } from 'src/app/common/services/data.service';
import { IsLoadingService } from '@service-work/is-loading';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  subData: Subscription;
  dataDisplay = {
    soLuongNguoiDung: 0,
    soLuongPhim: 0,
    soLuongCumRap: 0,
    soLuongRap: 0,
    sum: [0, 0, 0, 0, 0, 0, 0, 18000, 0, 0, 0, 0],
    maxValue: 3000
  };

  dataDailySalesChart: any = {
    labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
    series: []
  };

  optionsDailySalesChart: any = {
    lineSmooth: Chartist.Interpolation.cardinal({
      tension: 0
    }),
    low: 0,
    high: 0,
    chartPadding: { top: 0, right: 0, bottom: 0, left: 50 }
  };

  constructor(private dataService: DataService, private isLoadingService: IsLoadingService) {}

  startAnimationForLineChart(chart) {
    let seq: any, delays: any, durations: any;
    seq = 0;
    delays = 80;
    durations = 500;

    chart.on('draw', function(data) {
      if (data.type === 'line' || data.type === 'area') {
        data.element.animate({
          d: {
            begin: 600,
            dur: 700,
            from: data.path
              .clone()
              .scale(1, 0)
              .translate(0, data.chartRect.height())
              .stringify(),
            to: data.path.clone().stringify(),
            easing: Chartist.Svg.Easing.easeOutQuint
          }
        });
      } else if (data.type === 'point') {
        seq++;
        data.element.animate({
          opacity: {
            begin: seq * delays,
            dur: durations,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq = 0;
  }
  startAnimationForBarChart(chart) {
    let seq2: any, delays2: any, durations2: any;

    seq2 = 0;
    delays2 = 80;
    durations2 = 500;
    chart.on('draw', function(data) {
      if (data.type === 'bar') {
        seq2++;
        data.element.animate({
          opacity: {
            begin: seq2 * delays2,
            dur: durations2,
            from: 0,
            to: 1,
            easing: 'ease'
          }
        });
      }
    });

    seq2 = 0;
  }
  ngOnInit() {
    this.getData();

    var dailySalesChart = new Chartist.Line('#dailySalesChart', this.dataDailySalesChart, this.optionsDailySalesChart);

    this.startAnimationForLineChart(dailySalesChart);
  }

  getData() {
    const uriDataDashBoard = 'QuanLyNguoiDung/LayThongTinDashboard';
    this.subData = this.isLoadingService.add(
      this.dataService.getAPI(uriDataDashBoard).subscribe((data: any) => {
        this.dataDisplay = data;

        console.log(data);

        this.dataDailySalesChart.series = [this.dataDisplay.sum];
        this.optionsDailySalesChart.high = this.dataDisplay.maxValue;
        var dailySalesChart = new Chartist.Line(
          '#dailySalesChart',
          this.dataDailySalesChart,
          this.optionsDailySalesChart
        );

        this.startAnimationForLineChart(dailySalesChart);
      })
    );
  }
}

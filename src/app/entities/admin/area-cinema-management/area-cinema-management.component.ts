import { Component, OnInit, ViewChild } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/common/services/data.service';
import { MatSort, MatPaginator, MatTableDataSource } from '@angular/material';
import { ShareService } from 'src/app/common/services/share.service';
import { TypeNotify } from 'src/app/common/models/type.arlet';
import { IsLoadingService } from '@service-work/is-loading';
import { CumRap } from 'src/app/common/models/cumRapVM';
import { HeThongRapVM } from 'src/app/common/models/heThongRapVM';

@Component({
  selector: 'app-area-cinema-management',
  templateUrl: './area-cinema-management.component.html',
  styleUrls: ['./area-cinema-management.component.scss']
})
export class AreaCinemaManagementComponent implements OnInit {
  subDanhSachCumRap: Subscription;
  subDanhSachHTR: Subscription;
  subCumRap: Subscription;

  cumRap: CumRap = new CumRap();
  isEdit = false;
  isInsert = false;

  title = 'Danh sách cụm rạp';
  subTitle = 'Hihi';

  danhSachCumRap: CumRap[] = [];
  displayedColumns: string[] = ['maCumRap', 'tenCumRap', 'thongTin', 'maHeThongRap', 'actions'];
  heThongRap: HeThongRapVM[] = [];

  dataSource = new MatTableDataSource<any>([]);
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private dataService: DataService, private isLoadingService: IsLoadingService) {}

  getListHTR() {
    const uriDanhSachHTR = 'QuanLyRap/LayThongTinHeThongRap';
    this.subDanhSachHTR = this.isLoadingService.add(
      this.dataService.getAPI(uriDanhSachHTR).subscribe((data: HeThongRapVM[]) => {
        this.heThongRap = data;
      })
    );
  }

  getFullListCumRap() {
    const uriDanhSachCumRap = 'QuanLyRap/LayThongTinCumRapChieu';
    this.subDanhSachCumRap = this.isLoadingService.add(
      this.dataService.getAPI(uriDanhSachCumRap).subscribe((data: CumRap[]) => {
        console.log(data);
        this.danhSachCumRap = data;
        this.dataSource.data = data;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      })
    );
  }

  ngOnDestroy(): void {
    this.subDanhSachCumRap.unsubscribe();
    if (this.subDanhSachHTR != null) {
      this.subDanhSachHTR.unsubscribe();
    }
    if (this.subCumRap != null) {
      this.subCumRap.unsubscribe();
    }
  }

  ngOnInit() {
    this.getFullListCumRap();
    this.getListHTR();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  ngAfterViewInit(): void {
    this.dataSource.sort = this.sort;
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  backToFormData() {
    this.isEdit = false;
    this.isInsert = false;
    this.cumRap = new CumRap();
  }

  confirmUpdate() {
    if (this.isEdit == true) {
      const uriCapNhatCumRap = 'QuanLyRap/CapNhatThongTinRapChieu';

      this.isLoadingService.add(
        this.dataService.putAPI(uriCapNhatCumRap, this.cumRap).subscribe((data: any) => {
          this.getFullListCumRap();
          ShareService.showNotification('Cập nhật thông tin cụm rạp thành công', '', TypeNotify.success);
          this.isEdit = false;
        })
      );
    }

    if (this.isInsert == true) {
      const uriThemCumRap = 'QuanLyRap/ThemCumRapChieu';

      console.log(this.cumRap);

      this.isLoadingService.add(
        this.dataService.postAPI(uriThemCumRap, this.cumRap).subscribe((data: any) => {
          this.getFullListCumRap();
          ShareService.showNotification('Thêm cụm rạp thành công', '', TypeNotify.success);
          this.isInsert = false;
        })
      );
    }
  }

  insertCumRap() {
    this.isInsert = true;
    this.cumRap = new CumRap();
  }

  editCumRap(maCumRap: string) {
    const uriGetCumRap = `QuanLyRap/LayThongTinCumRapChieu?maCumRap=${maCumRap}`;
    this.subCumRap = this.dataService.getAPI(uriGetCumRap).subscribe((data: CumRap) => {
      this.cumRap = data;
      this.isEdit = true;
    });
  }

  xoaCumRap(maCumRap: string) {
    const uriDeleteCumRap = `QuanLyRap/XoaCumRapChieu?maCumRap=${maCumRap}`;
    ShareService.showMessageConfirm(
      () => {
        this.isLoadingService.add(
          this.dataService.deleteAPI(uriDeleteCumRap).subscribe(
            result => {
              this.getFullListCumRap();
            },
            error => {
              console.log(error);
            }
          )
        );
      },
      null,
      `Bạn có chắc chắn muốn xóa cụm rạp này không?`
    );
  }
}

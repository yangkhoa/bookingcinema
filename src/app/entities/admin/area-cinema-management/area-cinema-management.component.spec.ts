import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AreaCinemaManagementComponent } from './area-cinema-management.component';

describe('AreaCinemaManagementComponent', () => {
  let component: AreaCinemaManagementComponent;
  let fixture: ComponentFixture<AreaCinemaManagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AreaCinemaManagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AreaCinemaManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentsModule } from '../components/components.module';
import { MaterialModule } from 'src/app/shared/material/material.module';
import { ShareModule } from 'src/app/shared/share.module';
import { AreaCinemaManagementComponent } from './area-cinema-management.component';
import { AreaCinemaManagementRoutingModule } from './area-cinema-management-routing.module';

@NgModule({
  declarations: [AreaCinemaManagementComponent],
  imports: [CommonModule, ComponentsModule, MaterialModule, AreaCinemaManagementRoutingModule, ShareModule]
})
export class AreaCinemaManagementModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AreaCinemaManagementComponent } from './area-cinema-management.component';

const routes: Routes = [
  {
    path: '',
    component: AreaCinemaManagementComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AreaCinemaManagementRoutingModule {}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { IsLoginGuard } from '../../common/guards/is-login.guard';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    canActivate: [IsLoginGuard],
    children: [
      {
        path: '',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'quan-ly-nguoi-dung',
        loadChildren: './user-management/user-management.module#UserManagementModule'
      },
      {
        path: 'quan-ly-phim',
        loadChildren: './movie-management/movie-management.module#MovieManagementModule'
      },
      {
        path: 'quan-ly-rap',
        loadChildren: './cinema-management/cinema-management.module#CinemaManagementModule'
      },
      {
        path: 'quan-ly-cum-rap',
        loadChildren: './area-cinema-management/area-cinema-management.module#AreaCinemaManagementModule'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {}

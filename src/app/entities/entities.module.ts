import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntitiesComponent } from './entities.component';
import { EntitiesRoutingModule } from './entities-routing.module';
import { MaterialModule } from '../shared/material/material.module';

@NgModule({
  declarations: [EntitiesComponent],
  imports: [CommonModule, EntitiesRoutingModule, MaterialModule]
})
export class EntitiesModule {}

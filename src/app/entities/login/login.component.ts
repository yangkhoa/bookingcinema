import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Taikhoan } from 'src/app/common/models/taiKhoanVM';
import { DataService } from 'src/app/common/services/data.service';
import { Router } from '@angular/router';
import { TypeNotify } from 'src/app/common/models/type.arlet';
import { IsLoadingService } from '@service-work/is-loading';
import { ShareService } from 'src/app/common/services/share.service';
import { config } from 'src/app/common/config/web.config';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  isLoginForm: boolean = true;
  subLogin: Subscription;
  subRegister: Subscription;
  user = new Taikhoan();
  repassword: string;

  constructor(
    private router: Router,
    private shareServices: ShareService,
    private dataServices: DataService,
    private isLoadingService: IsLoadingService
  ) {}

  dangNhap() {
    const uriDangNhap = 'QuanLyNguoiDung/DangNhap';

    this.subLogin = this.isLoadingService.add(
      this.dataServices.postAPI(uriDangNhap, this.user).subscribe((data: any) => {
        localStorage.setItem(config.userLogin, JSON.stringify(JSON.parse(data)));

        this.shareServices.currentUserSubject.next(JSON.parse(data));

        if (JSON.parse(data).maLoaiNguoiDung == '001') {
          this.router.navigate(['/admin']);
          ShareService.showNotification('Đăng nhập thành công', '', TypeNotify.success);
        }
        if (JSON.parse(data).maLoaiNguoiDung == '002') {
          this.router.navigate(['/home']);
          ShareService.showNotification('Đăng nhập thành công', '', TypeNotify.success);
        }
      })
    );
  }

  dangKy() {
    this.user.maLoaiNguoiDung = '002';
    const uriDangKy = 'QuanLyNguoiDung/DangKy';

    this.subRegister = this.isLoadingService.add(
      this.dataServices.postAPI(uriDangKy, this.user).subscribe((data: any) => {
        localStorage.setItem(config.userLogin, JSON.stringify(JSON.parse(data)));

        this.router.navigate(['/home']);
        ShareService.showNotification(
          'Chúc mừng bạn đã đăng ký thành công. Hệ thống đã tự động đăng nhập giúp bạn',
          '',
          TypeNotify.success
        );

        this.shareServices.currentUserSubject.next(JSON.parse(localStorage.getItem(config.userLogin)));
      })
    );
  }

  ngOnInit() {
    if (this.router.url === '/register') {
      this.isLoginForm = false;
    }
  }
}
